@extends('master.app')

@section('navigasi')

    <!-- Page Header Start -->
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>About Yura Regita !</h2>
                </div>
                <div class="col-12">
                    <a href="">About Me</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->


    <!-- About Start -->
    <div class="about">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-6">
                    <div class="about-img">
                        <img src="img/about.jpg" alt="Image">
                    </div>
                </div>
                <div class="col-lg-7 col-md-6">
                    <div class="section-header text-left">
                        <p>Learn About Me</p>
                        <h2>Ni Putu Yura Regita Cahyani</h2>
                    </div>
                    <div class="about-text">
                        <div class="section-header text-left">  
                            <p>
                                Lahir di Temukus pada tanggal 17 Juni 2001, remaja yang akrab di panggil Yura ini kini berusia 20 tahun. Saat ini Yura duduk di bangku kuliah dengan
                                mengambil Program Studi Sistem Informasi di Universitas Pendidikan Ganesha. Memiliki hobby dalam hal
                                seni seperti menari dan bernyanyi, membuat Yura mengikuti salah satu Unit Kegiatan Mahasiswa di Universitas.
                            </p>
                            <p>
                                Beralamat tinggal di Dusun Pegayaman, Desa Temukus, anak pertama dari 3 bersaudara ini memiliki 2 saudara perempuan
                                yang bernama Ni Made Yuna Pregina Tiari dan Ni Nyoman Apriska Hana Sucila. Ayah I Nyoman Susrama bekerja sebagai karyawan swasta
                                di salah satu perusahaan di Singaraja dan Ibu Ni Nyoman Saniati bekerja sebagai pedagang. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About End -->

@endsection 